#!/usr/bin/python

import sys

#filename = "../testData/one" 
filename = sys.argv[1]

# first IO
lines = open(filename).readlines()
(init, amount, goal) = lines[0].split(';')
goal = goal.strip();
amount = int(amount)
nb = int(lines[1])

##################
# parsing
currencies = {}
for i in range(2, nb + 1):
    (currencyOne, currencyTwo, rate) = lines[i].split(';')
    if not(currencyOne in currencies):
        currencies[currencyOne] = { 'open': True, 'correspondances': {}}
    if not(currencyTwo in currencies):
        currencies[currencyTwo] = { 'open': True, 'correspondances': {}}
    currencies[currencyOne]['correspondances'][currencyTwo] = float(rate)
    currencies[currencyTwo]['correspondances'][currencyOne] = 1 / float(rate)

###################
# algorithm (kind of Djikstra)
open = [(init, amount)]
while(len(open) > 0):
    (name, amount) = open.pop(0)
    if name == goal:
        result = amount
        break
    current = currencies[name]
    #print(current)
    if current['open']:
        for (corr, rate) in current['correspondances'].items():
            open.append((corr, amount * rate))
    current['open'] = False

print("resultat:", result)
