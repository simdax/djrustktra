# Simple Djikstra traversal built in Rust, with a strange name.

You can build the binary with cargo and give it a command
* cargo run testData/one,

or run some tests.
* cargo run --example one

or simply `cargo build` and take the binary in 'target'
