mod lib;
use lib::{Parser};

fn main(){
    // success
    let mut parser = Parser::new("testData/one");
    let result = parser.solve();
    println!("result for example: {:.4}", result);

    // success
    let mut parser = Parser::new("testData/one_half");
    let result = parser.solve();
    println!("result with half the init value: {:.4}", result);

    // success
    let mut parser = Parser::new("testData/one_reverse");
    let result = parser.solve();
    println!("result from JPY to EUR: {:.4}", result);

    // Failing test
    let mut parser = Parser::new("testData/two");
    let result = parser.solve();
    println!("failing test: {:.4}", result);
    
    // crashing  :*(
//    let mut parser = Parser::new("testData/nofile");
}

