use std::env;

mod lib;
use lib::{
    Parser
};

fn main(){
    let path:Vec<String> = env::args().collect();
    assert_ne!(path.len(), 1, "miss argument");
    let mut parser = Parser::new(&path[1]);
    let result = parser.solve();
    if result == -1.0
    {
        println!("unable to solve the file");
    }
    else
    {
        println!("result: {:.4}", result);
    }
}
