/// Here is the main parser structure.
/// It reads from file, parse it's content and find the solution.

use std::fs::File;
use std::io::{BufRead, BufReader};

mod data_structures;
pub use data_structures::*;

/// simple Helper to get lines from file name.
pub fn read_lines<'a>(path: &'a str) -> Vec<String> {
    let file = File::open(path).expect("can't open file {}");
    let buf = BufReader::new(file);
    buf.lines().map(|line| {
        line.unwrap()
    }).collect()
}

/// This struct represents every
#[derive(Debug, Clone)]
pub struct Init { 
    pub init_currency: String, 
    pub final_currency: String,
    pub amount: u32 
}

/// This struct is used to create each node of the path in the graph traversal.
/// It will keep the name of the next node, and copy the amount to return it at the end.
struct Node {
    name: String,
    amount: f32,
}

/// This struct is the main structure of the application. It represents the structure
/// of the file, and implements the djikstra graph traversal algorithm.
pub struct Parser
{
    pub init: Option<Init>,
    pub nb:  Option<u32>,
    pub currencies: Currencies,
}

impl Parser{
    pub fn new(path: &str) -> Self {
        let lines = read_lines(path);
        let mut parser = Parser{
            init:       None,
            nb:         None,
            currencies: Currencies::new(),
        };
        parser.parse(lines);
        parser
    }

    /// TODO: refacto the split function
    pub fn parse(& mut self, lines: Vec<String>)  {
        for (i, line) in lines.iter().enumerate() {
            match i {
                0 => {
                    let words = line.split(";").collect::<Vec<&str>>();
                    assert_eq!(words.len(), 3);//, format!("first line is not well formatted: {}", line));
                    self.init = Some(Init {
                        init_currency: words[0].to_string(), 
                        final_currency: words[2].to_string(), 
                        amount: words[1].parse::<u32>().expect("amount seems not a number")
                    });
                },
                1 => self.nb = Some(line.parse().expect("second line seems no to be a number")),
                _ => {
                    let words = line.split(";").collect::<Vec<&str>>();
                    assert_eq!(words.len(), 3);
                    self.currencies.add_currency( 
                        words[0].to_string(), 
                        words[1].to_string(),
                        words[2].parse::<f32>().expect("amount seems not a number"));
                }
            }
        }
    }

    /// A simple breadth first graph traversal. As We don't search the best way, we
    /// consider only
    pub fn solve(&mut self) -> f32 {
        if let Some(init) = self.init.clone() {
            let mut open = vec![
                Node { name: init.init_currency, amount: init.amount as f32 }
            ];

            while open.len() > 0 {
                let node = open.pop().unwrap();
                if node.name == init.final_currency {
                    return node.amount;
                }
                let currency = &mut self.currencies.all.get_mut(&node.name).unwrap();
                if currency.open {
                    for (name, rate) in currency.conversions.clone() {
                        open.push( Node { 
                            name,
                            //amount: math.round(node.amount * rate, 4)  // todo
                            amount: node.amount * rate
                        });
                    }
                }
                currency.open = false;
            }
        }
        return -1.0;
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn open_file(){
        let lines = read_lines("testData/one");
        let count = lines.into_iter().count();
        assert_eq!(count, 8);
    }
    #[test]
    #[allow(non_snake_case)]
    fn parser_test(){
        let parser = Parser::new("testData/one");
        assert_eq!(parser.nb, Some(6));
        if let Some(init) = parser.init {
            assert_eq!(init.init_currency, "EUR");
            assert_eq!(init.final_currency, "JPY");
            assert_eq!(init.amount, 550);
        }
        else {
            panic!("no init struct");
        }
        assert_eq!(parser.currencies.all.keys().len(), 7, "{:?}", parser.currencies.all.keys());
        let AUD = parser.currencies.all.get("AUD")
            .expect("no AUD");
        assert_eq!(AUD.conversions.keys().len(), 2);
    }
}

