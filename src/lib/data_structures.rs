/// Main data structure module.
/// Defines :
///     * a currency as a dictionary of form label -> 
///     * a currency's collection, as another dictionary of form label -> currency
///
/// Remarks:
///  I added directly a 'open' field in currency, which will be used for graph traversal.

use std::collections::HashMap;

/// 
#[derive(Debug)]
pub struct Currencies {
    pub all: HashMap<String, Currency>,
}

impl Currencies { 
    pub fn new() -> Self { 
        Currencies { all: HashMap::new() } 
    }
   
    /// add a Currency, AND it's inverse
    ///
    pub fn add_currency(&mut self, from: String, to: String, value: f32)
    {
        self.upsert(from.clone(), to.clone(), value);
        self.upsert(to.clone(), from.clone(), 1.0 / value);
    }

    fn upsert(&mut self, from: String, to: String, value: f32){
        self.all.entry(from)
                .and_modify(|currency| { currency.conversions.insert(to.clone(), value); })
                .or_insert(Currency::new(to.clone(), value));
    }
}

#[derive(Debug)]
pub struct Currency {
    pub open: bool,
    pub conversions: HashMap<String, f32>,
}

impl Currency {
    pub fn new(name: String, value: f32) -> Self {
        let mut conversions = HashMap::new();
        conversions.insert(name, value);
        Currency { open: true, conversions }
    }
}

#[cfg(test)]
mod test
{
    use super::*;

    #[test]
    #[allow(non_snake_case)]
    fn currencies_insert(){
        let mut currencies = Currencies::new();
        currencies.add_currency("A".to_string(), "B".to_string(), 33.0);
        currencies.add_currency("A".to_string(), "C".to_string(), 35.0);
        let A = currencies.all.get("A").unwrap();
        assert_eq!(A.conversions.get("B").unwrap(), &33.0);
        assert_eq!(A.conversions.get("C").unwrap(), &35.0);
        let B = currencies.all.get("B").unwrap();
        assert_eq!(B.conversions.get("A").unwrap(), &(1.0 / 33.0));
    }
} 
